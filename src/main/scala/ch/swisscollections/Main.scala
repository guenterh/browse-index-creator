/*
 * Media Converter
 * Extracts media files from Fedora repository
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.swisscollections

import java.time.Duration
import java.util.UUID

import ch.swisscollections.alphabeticbrowse.indexcreator.dbclient.{DBClientWrapper, DBWrapperResult}
import org.apache.kafka.clients.consumer.{ConsumerConfig, ConsumerRecord, KafkaConsumer}
import org.apache.logging.log4j.scala.Logging
import org.memobase.settings.SettingsLoader

import scala.jdk.CollectionConverters._


object Main extends App with Logging {
  val settings = new SettingsLoader(List(
    "jdbcDriver",
    "jdbcConnection",
    "user",
    "passwd"
  ).asJava,
    "app.yml",
    false,
    false,
    true,
    false)


  logger.info("starting service with the following config settings")
  settings.getAppSettings.forEach((key,value) => logger.info(s"key: $key - value: $value"))
  settings.getKafkaConsumerSettings.forEach((key,value) => logger.info(s"key: $key - value: $value"))


  val dbClient = new DBClientWrapper(
    settings.getAppSettings.getProperty("jdbcDriver"),
    settings.getAppSettings.getProperty("jdbcConnection"),
    settings.getAppSettings.getProperty("user"),
    settings.getAppSettings.getProperty("passwd")
  )




  val pollTime = 3000 //in ms
  var props = settings.getKafkaConsumerSettings
  //props.put("group.id",UUID.randomUUID().toString )
  val consumer = new KafkaConsumer[String, String](props)

  try {
    logger.debug(s"Subscribing to topic ${settings.getInputTopic}")
    consumer.subscribe(List(settings.getInputTopic).asJava)

    while (true) {
      val itMessages = consumer.poll(Duration.ofMillis(pollTime)).asScala
      val itBrowseValues: Iterable[(String, String)] = for (v <- itMessages)
        yield (v.key(),v.value())
      if (itBrowseValues.nonEmpty) {

        itBrowseValues.foreach(messageTupleKeyValue =>
          dbClient.processBrowseValue(messageTupleKeyValue._1, messageTupleKeyValue._2)
        )

      }
      else {
        /*
        solrWrapper.checkLatestCommit() match {
          case Committed =>
            logger.info("""committing to kafka after smaller chunk of solr
              | bulk docs were sent to server because time exceeded...""")
            consumer.commitSync()
          case _ => logger.info("no data for commiting to Solr")

         */
        println("in else")


      }

    }

  } catch {
    case e: Exception =>
      logger.error(e)
      sys.exit(1)
  } finally {
    logger.info("Shutting down application")
    consumer.close()
  }


}
