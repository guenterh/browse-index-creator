package ch.swisscollections.alphabeticbrowse.indexcreator.dbclient;


import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.util.Arrays;
import java.util.HashMap;
import org.apache.commons.codec.binary.Base64;

//only a temporary solution for this quick prototype to use most of the available vufind code
//should be implemented in a decent Scala type
public class DBClientWrapper {

    private static Connection dbmsConnection = null;
    private static HashMap<String, PreparedStatement> prepStats = new HashMap<String, PreparedStatement>();
    private String KEY_SEPARATOR = "\u0001";

    public DBClientWrapper(String jdbcDriver,
                           String jdbcConnection,
                           String user,
                           String password)  {

        initializeDBConnection(jdbcDriver,
                jdbcConnection,
                user,
                password);


    }




    private void initializeDBConnection(String jdbcDriver,
                                        String jdbcConnection,
                                        String user,
                                        String password) {
        //todo: create connection Pool
        try {

            Class.forName(jdbcDriver).getDeclaredConstructor().newInstance();

            dbmsConnection = DriverManager.getConnection(jdbcConnection, user, password);
            prepareStatement(dbmsConnection);


        } catch (ClassNotFoundException | InstantiationException |
                IllegalAccessException | SQLException |
                NoSuchMethodException | InvocationTargetException ex ) {
            Arrays.stream(ex.getStackTrace()).forEach(
                    stackeelem -> System.out.println(stackeelem.toString())
            );
            throw new RuntimeException("setup of database connection wasn't possible");

        }

    }



    public DBWrapperResult processBrowseValue(String messageKey,String browseValue) {

        System.out.println(messageKey);
        System.out.println(browseValue);
        String[] fields = browseValue.split(KEY_SEPARATOR);


        if (fields.length == 3) {
            // If we found the separator character, we have a key/value pair of
            // Base64-encoded strings to decode and push into the batch:

            String f1 = new String(Base64.decodeBase64(fields[1].getBytes()));
            String f2 = new String(Base64.decodeBase64(fields[2].getBytes()));
            try {

                PreparedStatement exists = prepStats.get("exists");

                exists.setBytes(1, Base64.decodeBase64(fields[0].getBytes()));


                //todo: look for a smarter jdbc query.. - perhaps something like exists?
                ResultSet rs = exists.executeQuery();

                int count = 0;
                while (rs.next()) {
                    count = rs.getInt("singlerow");
                }

                if (count == 0) {
                    PreparedStatement statement = prepStats.get("insertHeading");
                    statement.setBytes(1, Base64.decodeBase64(fields[0].getBytes()));
                    statement.setString(2, f1);
                    statement.setString(3, f2);
                    statement.executeUpdate();
                }

            } catch (SQLException sqlExc) {
                sqlExc.printStackTrace(System.out);
                System.out.println(sqlExc.getMessage());
                Arrays.stream(sqlExc.getStackTrace()).forEach(
                        stackeelem -> System.out.println(stackeelem.toString())
                );
            }


            //prep.addBatch();
        }



        //Todo: by now no caching - only prototype
        return DBWrapperResult.Continue;
    }



    private static void prepareStatement (Connection dbConnection) {

        PreparedStatement tempPrepared = null;
        StringBuilder errorMessage;

        try {
            tempPrepared = dbConnection.prepareStatement(
                    "insert into all_headings (`key`, `key_text`, `heading`) values (?, ?, ?)" );

            prepStats.put("insertHeading", tempPrepared);

            tempPrepared = dbConnection.prepareStatement(
                    //  "SELECT EXISTS(SELECT 1 FROM all_headings WHERE `key` = ? LIMIT 1)" );

            "SELECT count(*) as singlerow FROM all_headings WHERE `key` = ? " );

            prepStats.put("exists", tempPrepared);





        } catch (SQLException sqlExc) {
            Arrays.stream(sqlExc.getStackTrace()).forEach(
                    stackeelem -> System.out.println(stackeelem.toString())
            );
        }


    }





}
