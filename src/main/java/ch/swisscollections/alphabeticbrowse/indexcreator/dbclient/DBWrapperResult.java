package ch.swisscollections.alphabeticbrowse.indexcreator.dbclient;

public enum DBWrapperResult {

    Committed,
    Continue

}
