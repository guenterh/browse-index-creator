#!/usr/bin/env bash

#simple utility to set environment variables for local development
export KAFKA_BOOTSTRAP_SERVERS=localhost:9092,localhost:9093,localhost:9094
export CLIENT_ID=app.browse-index-creator
export TOPIC_IN=swisscollections-browse-values
export GROUP_ID=browse-index-creator.group
export ENABLE_AUTO_COMMIT='false'
export JDBC_DRIVER='org.mariadb.jdbc.Driver'
export JDBC_CONNECTION='jdbc:mysql://localhost:3306/swisscollections'
export USER='swisscollections'
export PASSWORD='han123'