CREATE DATABASE `swisscollections` /*!40100 DEFAULT CHARACTER SET utf8 */
use swisscollections;

/*
install mariadb 10.5 on ubuntu
*/

CREATE TABLE `all_headings` (
/* fuer binary max length 255 */
  `key` binary(10000) ,
  `key_text` TEXT,
  `heading` TEXT,
  KEY `key` (`headings_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `all_headings` (
  `key` BLOB ,
  `key_text` TEXT,
  `heading` TEXT,
  KEY `headings_key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8


CREATE USER 'swisscollections'@'%' IDENTIFIED BY 'han123';
CREATE USER 'swisscollections'@'localhost' IDENTIFIED BY 'han123';
GRANT ALL PRIVILEGES ON swisscollections.* TO 'swisscollections'@'%'  WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON swisscollections.* TO 'swisscollections'@'localhost'  WITH GRANT OPTION;